import React from 'react';
import { NavLink } from 'react-router-dom';
import s from './Aside.module.css';

export const Aside = () => (
  <nav className={s.nav}>
    <ul className={s.nav__list}>
      <li className={s.nav__item}>
        <NavLink to="/profile" activeClassName={s.active}>
          Profile
        </NavLink>
      </li>
      <li className={s.nav__item}>
        <NavLink to="/messages" activeClassName={s.active}>
          Messages
        </NavLink>
      </li>
      <li className={s.nav__item}>
        <NavLink to="/news" activeClassName={s.active}>
          News
        </NavLink>
      </li>
      <li className={s.nav__item}>
        <NavLink to="/music" activeClassName={s.active}>
          Music
        </NavLink>
      </li>
      <li className={s.nav__item}>
        <NavLink to="/settings" activeClassName={s.active}>
          Settings
        </NavLink>
      </li>
    </ul>
  </nav>
);
