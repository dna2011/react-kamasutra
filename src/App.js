import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import { Header } from './components/Layout/Header';
import { Aside } from './components/Layout/Aside';
import { Profile } from './pages/Profile/Profile';
import { MessagesContainer } from './pages/Messages/MessagesContainer';
import { News } from './pages/News/News';
import { Music } from './pages/Music/Music';
import { Settings } from './pages/Settings/Settings';

const App = (props) => {
  const withDataProfile = () => <Profile />;
  const withDataDialogs = () => <MessagesContainer />;

  return (
    <BrowserRouter>
      <div className="app-wrapper">
        <Header />
        <aside className="app-aside">
          <Aside />
        </aside>
        <main role="main" className="app-content">
          <Switch>
            <Route path="/profile" component={withDataProfile} />
            <Route path="/messages" component={withDataDialogs} />
            <Route path="/news" component={News} />
            <Route path="/music" component={Music} />
            <Route path="/settings" component={Settings} />
          </Switch>
        </main>
      </div>
    </BrowserRouter>
  );
};

export default App;
