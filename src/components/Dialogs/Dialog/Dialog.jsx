import React from 'react';
import { NavLink } from 'react-router-dom';
import s from './Dialog.module.css';

export const Dialog = (props) => {
  return (
    <li className={s.dialog}>
      <NavLink to={`/messages/${props.id}`} activeClassName={s.active}>
        {props.name}
      </NavLink>
    </li>
  );
};
