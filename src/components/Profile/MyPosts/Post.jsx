import React from 'react';
import s from './Post.module.css';

export const Post = (props) => (
  <div className={s.item}>
    <img
      src="https://www.kinonews.ru/insimgs/2019/newsimg/newsimg87089.jpg"
      alt="Автор поста"
    />
    {props.text}
    <span>Likes: {props.likesCount}</span>
  </div>
);
