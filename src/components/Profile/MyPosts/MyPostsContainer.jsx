import { connect } from 'react-redux';
import {
  addPostActionCreator,
  updatePostTextActionCreator,
} from '../../../redux/profileReducer';
import { MyPosts } from './MyPosts';

const mapStateToProps = (state) => {
  return {
    posts: state.profile.posts,
    postText: state.profile.postText,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdatePostText: (text) => {
      dispatch(updatePostTextActionCreator(text));
    },
    onSendPost: () => {
      dispatch(addPostActionCreator());
    },
  };
};

export const MyPostsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyPosts);
