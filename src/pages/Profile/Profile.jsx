import React, { Fragment } from 'react';
import { ProfileInfo } from '../../components/Profile/ProfileInfo/ProfileInfo';
import { MyPostsContainer } from '../../components/Profile/MyPosts/MyPostsContainer';

import s from './Profile.module.css';

export const Profile = (props) => (
  <Fragment>
    <ProfileInfo />
    <MyPostsContainer />
  </Fragment>
);
