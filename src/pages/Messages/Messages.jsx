import React from 'react';
import s from './Messages.module.css';
import { Dialog } from '../../components/Dialogs/Dialog/Dialog';
import { Message } from '../../components/Dialogs/Message/Message';

export const Messages = (props) => {
  const messageText = props.newMessageText;

  const clickHandler = () => {
    props.onSendMessage();
  };

  const changeHandler = (e) => {
    const newText = e.target.value;
    props.onChangeMessage(newText);
  };

  return (
    <div className={s.dialogs}>
      <ul className={s.dialogsList}>
        {props.dialogs.map((d) => (
          <Dialog id={d.id} name={d.name} key={d.id} />
        ))}
      </ul>
      <div className={s.messages}>
        <div>
          {props.messages.map((m) => (
            <Message text={m.text} key={m.id} />
          ))}
        </div>
        <div>
          <textarea
            placeholder="Enter your message"
            value={messageText}
            onChange={changeHandler}
          />
          <button onClick={clickHandler}>Send</button>
        </div>
      </div>
    </div>
  );
};
