const ADD_POST = 'ADD-POST';
const UPDATE_POST_TEXT = 'UPDATE-POST-TEXT';

const initialState = {
  postText: '',
  posts: [
    { id: 1, text: '123123', likesCount: 27 },
    { id: 2, text: 'Привет', likesCount: 52 },
    { id: 3, text: 'Пока', likesCount: 21 },
    { id: 4, text: 'Дима', likesCount: 22 },
  ],
};

export const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST:
      const newPost = {
        id: Date.now(),
        text: state.postText,
        likesCount: Math.floor(Math.random() * 50),
      };
      return {
        ...state,
        posts: [...state.posts, newPost],
        postText: '',
      };
    case UPDATE_POST_TEXT:
      return {
        ...state,
        postText: action.text,
      };
    default:
      return state;
  }
};

export const addPostActionCreator = () => ({ type: ADD_POST });

export const updatePostTextActionCreator = (text) => ({
  type: UPDATE_POST_TEXT,
  text,
});
