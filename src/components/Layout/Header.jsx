import React from 'react';
import s from './Header.module.css';

export const Header = () => (
  <header className={s['app-header']}>
    <img
      src="https://lh3.googleusercontent.com/proxy/dZ3a3_VwqO9DCyMtOqyjHJ4gxkpA44GLV89vW9Kz-EIqB0AtBEJklMwj_TFpLOVzwxVb6PnubarR7Wt-Ed-7fHqNVPpIe6v4S2tQYfdYkYin2nZ2fwbUnA"
      alt="Our logo"
    />
  </header>
);
