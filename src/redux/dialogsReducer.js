const UPDATE_MESSAGE_TEXT = 'UPDATE-MESSAGE-TEXT';
const SEND_MESSAGE = 'SEND-MESSAGE';

const initialSatte = {
  dialogs: [
    { id: 1, name: 'Nadya' },
    { id: 2, name: 'Dad' },
    { id: 3, name: 'Mom' },
    { id: 4, name: 'Koks' },
  ],

  messages: [
    { id: 1, text: 'First message' },
    { id: 2, text: 'Second message' },
    { id: 3, text: 'Third message' },
    { id: 4, text: 'Fourth message' },
  ],

  newMessageText: '',
};

export const dialogsReducer = (state = initialSatte, action) => {
  switch (action.type) {
    case UPDATE_MESSAGE_TEXT:
      return {
        ...state,
        newMessageText: action.text,
      };
    case SEND_MESSAGE:
      const newMessage = {
        id: Date.now(),
        text: state.newMessageText,
      };
      return {
        ...state,
        messages: [...state.messages, newMessage],
        newMessageText: '',
      };
    default:
      return state;
  }
};

export const addMessageActionCreator = () => ({ type: SEND_MESSAGE });

export const updateMessageActionCreator = (text) => ({
  type: UPDATE_MESSAGE_TEXT,
  text,
});
