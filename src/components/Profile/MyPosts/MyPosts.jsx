import React from 'react';
import { Post } from './Post';
import s from './MyPosts.module.css';

export const MyPosts = (props) => {
  const postsTemplate = props.posts.map((p) => (
    <Post key={p.id} text={p.text} likesCount={p.likesCount} />
  ));

  const newPostElement = React.createRef();

  const clickHandler = () => {
    props.onSendPost();
  };

  const postChangeHandler = () => {
    const text = newPostElement.current.value;
    props.onUpdatePostText(text);
  };

  return (
    <div className={s.posts}>
      <h3>My posts</h3>
      <div>
        <textarea
          ref={newPostElement}
          value={props.postText}
          onChange={postChangeHandler}
        />
      </div>
      <button onClick={clickHandler}>Send!</button>
      {postsTemplate}
    </div>
  );
};
