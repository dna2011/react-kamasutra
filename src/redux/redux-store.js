import { createStore, combineReducers } from 'redux';
import { profileReducer } from './profileReducer';
import { dialogsReducer } from './dialogsReducer';

const reducersBunch = combineReducers({
  profile: profileReducer,
  messages: dialogsReducer,
});

const store = createStore(reducersBunch);

window.store = store;

export default store;
