import { profileReducer } from './profileReducer';
import { dialogsReducer } from './dialogsReducer';

const store = {
  _state: {
    profile: {
      postText: '',
      posts: [
        { id: 1, text: '123123', likesCount: 27 },
        { id: 2, text: 'Привет', likesCount: 52 },
        { id: 3, text: 'Пока', likesCount: 21 },
        { id: 4, text: 'Дима', likesCount: 22 },
      ],
    },

    messages: {
      dialogs: [
        { id: 1, name: 'Nadya' },
        { id: 2, name: 'Dad' },
        { id: 3, name: 'Mom' },
        { id: 4, name: 'Koks' },
      ],

      messages: [
        { id: 1, text: 'First message' },
        { id: 2, text: 'Second message' },
        { id: 3, text: 'Third message' },
        { id: 4, text: 'Fourth message' },
      ],

      newMessageText: '',
    },
  },

  getState() {
    return this._state;
  },

  _callSubscriber() {
    console.warn('No observer');
  },

  subsccribe(cb) {
    this._callSubscriber = cb;
  },

  dispatch(action) {
    // action { tpye: string, ...payload }
    this._state.profile = profileReducer(this._state.profile, action);
    this._state.messages = dialogsReducer(this._state.messages, action);

    this._callSubscriber(this._state);
  },
};

window.store = store;

export default store;
