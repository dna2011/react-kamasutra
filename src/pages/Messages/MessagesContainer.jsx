import { connect } from 'react-redux';
import {
  addMessageActionCreator,
  updateMessageActionCreator,
} from '../../redux/dialogsReducer';
import { Messages } from './Messages';

const mapStateToProps = (state) => {
  return {
    newMessageText: state.messages.newMessageText,
    dialogs: state.messages.dialogs,
    messages: state.messages.messages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSendMessage: () => {
      dispatch(addMessageActionCreator());
    },
    onChangeMessage: (text) => {
      dispatch(updateMessageActionCreator(text));
    },
  };
};

export const MessagesContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Messages);
